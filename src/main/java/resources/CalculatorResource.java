package resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Arrays;

/**
 * Calculator resource exposed at '/calculator' path
 */
@Path("/calculator")
public class CalculatorResource {

    /**
     * Method handling HTTP POST requests. The calculated answer to the expression will be sent to the client as
     * plain/text.
     * @param expression the expression to be solved as plain/text.
     * @return solution to the expression as plain/text or -1 on error
     */
    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public String calculate(String expression){
        // Removes all whitespaces
        String expressionTrimmed = expression.replaceAll("\\s+","");

        int result = -1;

        /*
         * .matches use regex expression to decide if a String matches a given pattern.
         * [0-9]+[+][0-9]+ explained:
         * [0-9]+: a number from 0-9 one or more times. For example 1, 12 and 123.
         * [+]: the operator + one time.
         * The total regex expression accepts for example:
         * 12+34,
         * 1+2,
         * 10000+1000
         */

        try {
            if (expressionTrimmed.matches("^\\d+(?:\\s*[+]\\s*\\d+)*$")) {
                return String.format("%d", sum(expressionTrimmed));
            } else if (expressionTrimmed.matches("^\\d+(?:\\s*[-]\\s*\\d+)*$")) {
                return String.format("%d", subtraction(expressionTrimmed));
            } else if (expressionTrimmed.matches("^\\d+(?:\\s*[*]\\s*\\d+)*$")) {
                return String.format("%d", multiplication(expressionTrimmed));
            } else if (expressionTrimmed.matches("^\\d+(?:\\s*[/]\\s*\\d+)*$")) {
                return String.format("%d", division(expressionTrimmed));
            }
        }catch (IllegalArgumentException ex){
            return ex.getMessage();
        }

        return "Please enter a valid expression";
    }

    private String[] trimSplit(String[] split){

        String[] result = new String[split.length];

        for(int i = 0;i<split.length;i++){
            result[i] = split[i].trim();
        }
        return result;
    }

    /**
     * Method used to calculate a sum expression.
     * @param expression the equation to be calculated as a String
     * @return the answer as an int
     */
    public int sum(String expression)throws NumberFormatException{
        String[] split = expression.split("[+]");

        split = trimSplit(split);

        int sum = Integer.parseInt(split[0]);
        String[] newSplit = Arrays.copyOfRange(split, 1, split.length);

        for(String s : newSplit){
            sum += Integer.parseInt(s);
        }

        return sum;
    }

    /**
     * Method used to calculate a subtraction expression.
     * @param expression the expression to be calculated as a String
     * @return the answer as an int
     */
    public int subtraction(String expression)throws NumberFormatException{
        String[] split = expression.split("[-]");

        split = trimSplit(split);

        int sum = Integer.parseInt(split[0]);
        String[] newSplit = Arrays.copyOfRange(split, 1, split.length);

        for(String s : newSplit){
            sum -= Integer.parseInt(s);
        }

        return sum;
    }

    /**
     * Method used to calculate a multiplication expression.
     * @param expression the expression to be calculated as a String
     * @return the answer as an int
     */
    public int multiplication(String expression)throws NumberFormatException{
        String[] split = expression.split("[*]");

        split = trimSplit(split);

        int sum = 1;

        for(String s : split){
            sum *= Integer.parseInt(s);
        }

        return sum;
    }

    /**
     * Method used to calculate a division expression.
     * @param expression the expression to be calculated as a String
     * @return the answer as an int
     */
    public int division(String expression)throws IllegalArgumentException {
        String[] split = expression.split("[/]");

        split = trimSplit(split);

        int sum = Integer.parseInt(split[0]);
        String[] newSplit = Arrays.copyOfRange(split, 1, split.length);

        int divisor;
        for(String s : newSplit){

            divisor = Integer.parseInt(s);

            if(divisor == 0) {
                throw new IllegalArgumentException("You cannot divide by zero");
            }

            sum /= divisor;
        }

        return sum;
    }
}
