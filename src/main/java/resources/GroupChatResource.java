package resources;

import dao.GroupChatDAO;
import data.GroupChat;
import data.Message;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;

/**
 * GroupChat resource exposed at "/groupchat" path
 */
@Path("/groupchat")
public class GroupChatResource {

    /**
     * GET method to get one groupchat with specified groupChatId
     * @param groupChatId of the chat to GET
     * @return GroupChat
     */
    @GET
    @Path ("{groupChatId}")
    @Produces (MediaType.APPLICATION_JSON)
    public GroupChat getGroupChat(@PathParam("groupChatId") int groupChatId){
        GroupChatDAO gcDAO = new GroupChatDAO();

        GroupChat gc = gcDAO.getGroupChat(groupChatId);
        gc.setMessageList(gcDAO.getGroupChatMessages(groupChatId));
        gc.setUserList(gcDAO.getGroupChatUsers(groupChatId));

        return gc;
    }

    @GET
    @Path("user/{userId}")
    @Produces (MediaType.APPLICATION_JSON)
    public ArrayList<GroupChat> getUserGroupChats(@PathParam("userId") int userId){
        GroupChatDAO gcDAO = new GroupChatDAO();
        return gcDAO.getGroupChatByUserId(userId);
    }

    @POST
    @Consumes (MediaType.APPLICATION_JSON)
    @Produces (MediaType.APPLICATION_JSON)
    public GroupChat postGroupChat(GroupChat gc){
        GroupChatDAO gcDAO = new GroupChatDAO();
        return gcDAO.addGroupChat(gc);
    }

    @GET
    @Path("{groupChatId}/message")
    @Produces (MediaType.APPLICATION_JSON)
    public ArrayList<Message> getGroupChatMessages(@PathParam("groupChatId") int groupChatId){
        GroupChatDAO gcDAO = new GroupChatDAO();
        return gcDAO.getGroupChatMessages(groupChatId);
    }

    @POST
    @Path("{groupChatId}/message")
    @Consumes (MediaType.APPLICATION_JSON)
    @Produces (MediaType.APPLICATION_JSON)
    public Message postMessage(@PathParam("groupChatId") int groupChatId, Message message){
        GroupChatDAO gcDAO = new GroupChatDAO();
        return gcDAO.addMessage(groupChatId, message);
    }



}
