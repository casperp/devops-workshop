import org.junit.Test;
import resources.CalculatorResource;

import static org.junit.Assert.assertEquals;

public class CalculatorResourceTest{

    @Test
    public void testCalculate(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300+50";
        assertEquals("450", calculatorResource.calculate(expression));

        expression = " 300 - 100 - 50";
        assertEquals("150", calculatorResource.calculate(expression));

        expression = "10*10 *10";
        assertEquals("1000", calculatorResource.calculate(expression));

        expression = "150/10/3";
        assertEquals("5", calculatorResource.calculate(expression));
    }

    @Test
    public void testSum(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300+1";
        assertEquals(401, calculatorResource.sum(expression));

        expression = "300+99+    1";
        assertEquals(400, calculatorResource.sum(expression));
    }

    @Test
    public void testSubtraction(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "999-100";
        assertEquals(899, calculatorResource.subtraction(expression));

        expression = "20-3";
        assertEquals(17, calculatorResource.subtraction(expression));
    }

    @Test
    public void testMultiplication(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "10*5";
        assertEquals(50, calculatorResource.multiplication(expression));

        expression = "-4*4";
        assertEquals(-16, calculatorResource.multiplication(expression));
    }

    @Test
    public void testDivision(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "10/5";
        assertEquals(2, calculatorResource.division(expression));

        expression = "42/7";
        assertEquals(6, calculatorResource.division(expression));


        expression = "42/0";
        try{
            assertEquals(6, calculatorResource.division(expression));
        }catch (IllegalArgumentException ex){
            assertEquals("You cannot divide by zero", ex.getMessage());
        }
    }
}
